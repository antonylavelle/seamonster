﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MeshCombiner : EditorWindow
{
	
	

	// Add menu item named "My Window" to the Window menu
	[MenuItem("Window/Scenic Mesh Combiner")]
	public static void ShowWindow()
	{
		//Show existing window instance. If one doesn't exist, make one.
		EditorWindow.GetWindow(typeof(MeshCombiner));
	}

    private static float vertsPerMesh = 6000;
	void OnGUI()
	{
        vertsPerMesh = EditorGUI.Slider(new Rect(0, 50, position.width, 20),"Verts per mesh", vertsPerMesh, 1000, 65535);

     
        if (GUILayout.Button("COMBINE SELECTED MESH"))
        {
			Debug.Log("MERGING MESHES");
			DateTime timeNow = System.DateTime.Now;


            var OriginalObjectReference = Selection.activeGameObject;
            OriginalObjectReference.SetActive(true);
            //make untagged gameobject with everything in it.
            CombinedGameObject = new GameObject();
            CombinedGameObject.name = OriginalObjectReference.name + "_Combined";
            CombinedGameObject.transform.SetParent(OriginalObjectReference.transform.parent);
            CombinedGameObject.transform.position = OriginalObjectReference.transform.position;
            CombinedGameObject.transform.rotation = OriginalObjectReference.transform.rotation;
            CombinedGameObject.transform.localScale = OriginalObjectReference.transform.localScale;

       

            MeshRenderer[] MeshRenderers = OriginalObjectReference.GetComponentsInChildren<MeshRenderer>(false);

            List<MeshRenderer> UntaggedMeshes = new List<MeshRenderer>();
            List<MeshRenderer> CombineNonStatic = new List<MeshRenderer>();

            for(int i = 0;i< MeshRenderers.Length;i++)
            {
                var meshRenderer = MeshRenderers[i];
                if(meshRenderer.gameObject.CompareTag("CombineNonStatic"))
                {
                    CombineNonStatic.Add(meshRenderer);
                }
                else
                {
                    UntaggedMeshes.Add(meshRenderer);
                }
            }
            //get all objects in untagged gameobject tagged low lightmap
            //repeat
            //combine seperately and set lightmap resolution.


            CombineMeshes(OriginalObjectReference, UntaggedMeshes.ToArray(),"Untagged", vertsPerMesh,0.1f,true);
            CombineMeshes(OriginalObjectReference, CombineNonStatic.ToArray(),"CombineNonStatic", vertsPerMesh,0.1f,false);
            TimeSpan timeTaken = (System.DateTime.Now) - timeNow;
			Debug.Log("Time to combine mesh is " + timeTaken.TotalMilliseconds);

            OriginalObjectReference.SetActive(false);
            OriginalObjectReference = null;
        }
	}


    private static List<MeshFilter> currentMeshFilters;
    private static Material[] sharedMaterial;

    private static GameObject GameObjectToCombine;
    private static GameObject CombinedGameObject;

    private static int  totalMeshCount = 0;

   
    private static List<MeshFilter> AllMeshFilters;
    private static List<MeshFilter> MeshFiltersLeftToUse;

    static int SortByMagnitude(MeshFilter p1, MeshFilter p2)
    {
        return 1;
        var farVector = new Vector3(5000, 0, 0);
        if(p1.sharedMesh==null || p2.sharedMesh == null)
        {
            Debug.Log("No shared mesh "+p1.gameObject.name+ " No shared mesh "+ p2.gameObject.name);
            return 1;
            
        }

        return (p1.sharedMesh.vertices[0]+farVector+p1.transform.position).magnitude.CompareTo((p2.sharedMesh.vertices[0] + farVector + p2.transform.position).magnitude);
    }

    


    public static void CombineMeshes(GameObject aGoRef, MeshRenderer[] meshRenderers,String label,float vertsPerMesh,float lightMapScale,bool makeStatic)
	{
     
        GameObjectToCombine = aGoRef;

        GameObjectToCombine.SetActive(true);



        totalMeshCount = 0;

		//MeshRenderer[] meshRenderers = GameObjectToCombine.GetComponentsInChildren<MeshRenderer>(false);
        if (meshRenderers != null && meshRenderers.Length > 0)
        {
            //testing efficient packing
            AllMeshFilters = new List<MeshFilter>();

            for (int i = 0; i < meshRenderers.Length; i++)
            {
                MeshFilter filter = meshRenderers[i].gameObject.GetComponent<MeshFilter>();
                sharedMaterial = meshRenderers[i].sharedMaterials;
                AllMeshFilters.Add(filter);
            }

        //still useful for a starting point for smart picking
        AllMeshFilters.Sort(SortByMagnitude);



            var timeOut = Time.realtimeSinceStartup + 45;
            while (AllMeshFilters.Count > 0)
            {

                if (Time.realtimeSinceStartup > timeOut)
                {
                    Debug.Log("MESH COMBINE TIME OUT!");
                    break;
                }

                currentMeshFilters = new List<MeshFilter>();

                MeshFilter leader = AllMeshFilters[0];

                while (leader.sharedMesh==null && AllMeshFilters.Count>0)
                {
                    AllMeshFilters.RemoveAt(0);
                    if (AllMeshFilters.Count > 0)
                    {
                        leader = AllMeshFilters[0];
                    }
                }

                if(AllMeshFilters.Count<=0)
                {
                    Debug.Log("No leaders with mesh filters left!");
                    return;
                }

                currentMeshFilters.Add(leader);
                AllMeshFilters.Remove(leader);

                int vertCount = leader.sharedMesh.vertexCount;
                while (vertCount < vertsPerMesh)
                {
                    MeshFilter follower = null;
                    float minDistance = 1000000000;
                    foreach (MeshFilter filter in AllMeshFilters)
                    {
                        float distance = Vector3.Distance(leader.transform.position, filter.transform.position);
                        if (distance < minDistance)
                        {
                            follower = filter;
                            minDistance = distance;
                        }
                    }
                    if (follower == null || follower.sharedMesh==null)//no follower found
                    {
                       // Debug.Log("No follower found");
                        break;
                    }
                    else
                    {
                      //  Debug.Log("Follower found at dist "+ minDistance);
                   
                        var verts = follower.sharedMesh.vertexCount;
                        if (vertCount + verts > vertsPerMesh)
                        {
                           // Debug.Log("Follower overflows vert limit");
                            break;//this would push you over the vert limit
                        }
                        else
                        {
                           // Debug.Log("Adding follower to mesh "+ (vertCount + verts)+"/"+vertsPerMesh);
                            currentMeshFilters.Add(follower);//add ready for merge
                            AllMeshFilters.Remove(follower);
                            vertCount += verts;
                        }
                    }
                }

                combineCurrentMesh(label,lightMapScale,makeStatic);
                totalMeshCount++;
            }

                //get leader mesh filter

                //loop through all meshes looking for closest

                //repeat until vert limit hit

                //remove all used filters from allmeshfilters
		}
		
		Debug.Log("TOTAL MESHES COMBINED IN MULTI STAGE IS " + totalMeshCount);
       
    }

	private static void combineCurrentMesh(String label,float lightMapScale,bool makeStatic)
	{
		Mesh mesh = new Mesh();

        mesh.name = "Combined Mesh " + totalMeshCount;

		Matrix4x4 myTransform = GameObjectToCombine.transform.worldToLocalMatrix;
		List<Vector3> vertices = new List<Vector3>();
		List<Vector3> normals = new List<Vector3>();
		List<Vector2> uv1s = new List<Vector2>();
		List<Vector2> uv2s = new List<Vector2>();
		Dictionary<Material, List<int>> subMeshes = new Dictionary<Material, List<int>>();

		if (currentMeshFilters != null && currentMeshFilters.Count > 0)
		{
			foreach (MeshFilter filter in currentMeshFilters)
			{
		
				if (filter != null && filter.sharedMesh != null)
				{
					MergeMeshInto(filter.sharedMesh, sharedMaterial, myTransform * filter.transform.localToWorldMatrix, vertices, normals, uv1s, uv2s, subMeshes);
				}
			}
		}

        mesh.vertices = vertices.ToArray();
        if (normals.Count > 0) mesh.normals = normals.ToArray();
        if (uv1s.Count > 0) mesh.uv = uv1s.ToArray();
        if (uv2s.Count > 0) mesh.uv2 = uv2s.ToArray();

        mesh.subMeshCount = subMeshes.Keys.Count;
		Material[] materials = new Material[subMeshes.Keys.Count];
		int mIdx = 0;
		foreach (Material m in subMeshes.Keys)
		{
			materials[mIdx] = m;
			mesh.SetTriangles(subMeshes[m].ToArray(), mIdx++);
		}

		if (currentMeshFilters != null && currentMeshFilters.Count > 0)
		{
			GameObject combinedGameObject = new GameObject();

			combinedGameObject.name = GameObjectToCombine.name + "_Combined_" + totalMeshCount+ " [" + label + "]";

            

			MeshRenderer meshRend = combinedGameObject.AddComponent(typeof(MeshRenderer)) as MeshRenderer;
			meshRend.sharedMaterials = materials;

			MeshFilter meshFilter = combinedGameObject.AddComponent(typeof(MeshFilter)) as MeshFilter;

            meshFilter.sharedMesh = mesh;

            combinedGameObject.isStatic = makeStatic;

            
            SerializedObject so = new SerializedObject(meshRend);
            so.FindProperty("m_ScaleInLightmap").floatValue = lightMapScale;
            so.ApplyModifiedProperties();

            combinedGameObject.layer = LayerMask.NameToLayer("CameraCollision");

            combinedGameObject.transform.localPosition = GameObjectToCombine.transform.localPosition;
            combinedGameObject.transform.localRotation = GameObjectToCombine.transform.localRotation;
            combinedGameObject.transform.localScale = GameObjectToCombine.transform.localScale;

            
            combinedGameObject.transform.SetParent(CombinedGameObject.transform);

            mesh.RecalculateBounds();

            Unwrapping.GenerateSecondaryUVSet(mesh);

            var mc = combinedGameObject.AddComponent(typeof(MeshCollider)) as MeshCollider;
            combinedGameObject.GetComponent<MeshCollider>().sharedMesh = null;
            combinedGameObject.GetComponent<MeshCollider>().sharedMesh = mesh;


        }

	}

    private static Vector3[] LodArray(List<Vector3> targ, int lodQuality)
    {
        List<Vector3> list = new List<Vector3>();
        for (int i = 0; i < targ.Count; i += lodQuality)
        {
            if (i < targ.Count)
            {
                list.Add(targ[i]);
            }
        }
        return list.ToArray();
    }

    private static Vector2[] LodArray(List<Vector2> targ, int lodQuality)
    {
        List<Vector2> list = new List<Vector2>();
        for (int i = 0; i < targ.Count; i += lodQuality)
        {
            if (i < targ.Count)
            {
                list.Add(targ[i]);
            }
        }
        return list.ToArray();
    }

    private static Vector3[] LodArray(Vector3[] targ,int lodQuality)
    {
        List<Vector3> list = new List<Vector3>();
        for(int i = 0;i<targ.Length;i+= lodQuality)
        {
            if (i < targ.Length)
            {
                list.Add(targ[i]);
             }
        }
        return list.ToArray();
    }
    private static Vector2[] LodArray(Vector2[] targ, int lodQuality)
    {
        List<Vector2> list = new List<Vector2>();
        for (int i = 0; i < targ.Length; i += lodQuality)
        {
            if (i < targ.Length)
            {
                list.Add(targ[i]);
            }
        }
        return list.ToArray();
    }
    private static int[] LodArray(int[] targ, int lodQuality)
    {
        List<int> list = new List<int>();
        for (int i = 0; i < targ.Length; i += lodQuality)
        {
            if (i < targ.Length)
            {
                list.Add(targ[i]);
            }
        }
        return list.ToArray();
    }

    private static void MergeMeshInto(Mesh meshToMerge, Material[] ms, Matrix4x4 transformMatrix, List<Vector3> vertices, List<Vector3> normals, List<Vector2> uv1s, List<Vector2> uv2s, Dictionary<Material, List<int>> subMeshes)
	{
		if (meshToMerge == null) return;
		int vertexOffset = vertices.Count;
		Vector3[] vs = meshToMerge.vertices;

		for (int i = 0; i < vs.Length; i++)
		{
			vs[i] = transformMatrix.MultiplyPoint3x4(vs[i]);
		}
		vertices.AddRange(vs);

		Quaternion rotation = Quaternion.LookRotation(transformMatrix.GetColumn(2), transformMatrix.GetColumn(1));
		Vector3[] ns = meshToMerge.normals;
		if (ns != null && ns.Length > 0)
		{
			for (int i = 0; i < ns.Length; i++) ns[i] = rotation * ns[i];
			normals.AddRange(ns);
		}

		Vector2[] uvs = meshToMerge.uv;
		if (uvs != null && uvs.Length > 0) uv1s.AddRange(uvs);
		uvs = meshToMerge.uv2;
        if (uvs != null && uvs.Length > 0)
        {
            uv2s.AddRange(uvs);
        }
        else
        {
            Debug.Log("No UV2s present");
        }

		for (int i = 0; i < ms.Length; i++)
		{
			if (i < meshToMerge.subMeshCount)
			{
				int[] ts = meshToMerge.GetTriangles(i);
				if (ts.Length > 0)
				{
					if (ms[i] != null && !subMeshes.ContainsKey(ms[i]))
					{
						subMeshes.Add(ms[i], new List<int>());
					}
                    if (ms[i] != null)
                    {
                        List<int> subMesh = subMeshes[ms[i]];
                        for (int t = 0; t < ts.Length; t++)
                        {
                            ts[t] += vertexOffset;
                        }

                        subMesh.AddRange(ts);
                    }
                    else
                    {
                        for (int t = 0; t < ts.Length; t++)
                        {
                            ts[t] += vertexOffset;
                        }
                    }
                        
                  
				}
			}
		}
	}
}
